package com.meuspring.start.entity.repository;

import com.meuspring.start.entity.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long>
{
    public Persona findByName(String name);
}
