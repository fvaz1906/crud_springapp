package com.meuspring.start;

import com.meuspring.start.entity.Persona;
import com.meuspring.start.entity.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/persona")
public class PersonaController
{
    @Autowired
    private PersonaRepository repository;

    public PersonaController(PersonaRepository personaRepository)
    {
        this.repository = personaRepository;
    }

    @GetMapping
    @ResponseBody
    public List<Persona> findAll()
    {
        return (List<Persona>) repository.findAll();
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    public Persona findOne(@PathVariable Long id)
    {
        return repository.findOne(id);
    }

    @GetMapping(path = "/name/{name}")
    @ResponseBody
    public Persona findByName(@PathVariable String name)
    {
        return repository.findByName(name);
    }

    @PostMapping
    @ResponseBody
    public Persona create(@RequestParam String name, @RequestParam Integer age)
    {
        Persona persona = new Persona(name, age);
        if (name != null && name.length() > 0 && age != null && age > 0)
        {
            repository.save(persona);
        }
        return persona;
    }

    @DeleteMapping(path = "/{id}")
    @ResponseBody
    public void destroy(@PathVariable Long id)
    {
        Persona persona = repository.findOne(id);
        if (persona != null)
        {
            repository.delete(id);
        }
    }

}
